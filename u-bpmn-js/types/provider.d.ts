import { UBpmnModeler } from "../modules/UBpmnModeler"
import { Canvas, Shape } from "./draw"
import { ElementRegistry } from "./element-registry"
import { EventBus } from "./event"
import { BpmnFactory, ElementFactory } from "./factory"
import { BpmnModdle } from "./moddle"
import { AlignmentType, Modeling } from "./modeling"

export interface ZoomScroll {

}
export interface AlignElements {
  _modeling: Modeling
  trigger(elements: Shape[], type: AlignmentType): void
  _alignmentPosition()
  _getOrientationDetails(): { axis: 'x' | 'y', dimension: 'width' | 'height' }
  _isType(): boolean
}

export interface Selection {
  _canvas: Canvas
  _eventBus: EventBus
  _selectedElements: Shape[]
  deselect(element: Shape): void
  get(): Shape[]
  isSelected(element: Shape): boolean
  select(elements: Shape, add: boolean): void
}
/**
 * bpmn供应商map
 */
export interface BPMNProviderMap {
  adaptiveLabelPositioningBehavior: any
  alignElements: AlignElements
  appendBehavior: any
  associationBehavior: any
  attachEventBehavior: any
  attachSupport: any
  autoPlace: any
  autoPlaceSelectionBehavior: any
  autoScroll: any
  bendpointMove: any
  bendpointMovePreview: any
  bendpointSnapping: any
  bendpoints: any
  boundaryEventBehavior: any
  bpmnAutoPlace: any
  bpmnAutoResize: any
  bpmnAutoResizeProvider: any
  bpmnCopyPaste: any
  bpmnDiOrdering: any
  bpmnDistributeElements: any
  bpmnFactory: BpmnFactory
  bpmnGridSnapping: any
  bpmnImporter: any
  bpmnInteractionEvents: any
  bpmnOrderingProvider: any
  bpmnRenderer: any
  bpmnReplace: any
  bpmnReplacePreview: any
  bpmnRules: any
  bpmnSearch: any
  bpmnUpdater: any
  bpmnjs: UBpmnModeler
  /**  */
  canvas: Canvas
  changeSupport: any
  clipboard: any
  commandStack: any
  config: any
  connect: any
  connectPreview: any
  connectSnapping: any
  connectionDocking: any
  connectionPreview: any
  connectionSegmentMove: any
  contextPad: any
  contextPadProvider: any
  copyPaste: any
  create: any
  createBehavior: any
  createDataObjectBehavior: any
  createMoveSnapping: any
  createParticipantBehavior: any
  createPreview: any
  dataInputAssociationBehavior: any
  dataStoreBehavior: any
  defaultRenderer: any
  deleteLaneBehavior: any
  detachEventBehavior: any
  directEditing: any
  distributeElements: any
  dragging: any
  dropOnFlowBehavior: any
  editorActions: any
  elementFactory: ElementFactory
  elementRegistry: ElementRegistry
  eventBasedGatewayBehavior: any
  eventBus: EventBus
  fixHoverBehavior: any
  globalConnect: any
  graphicsFactory: any
  gridSnapping: any
  gridSnappingAutoPlaceBehavior: any
  gridSnappingCreateParticipantBehavior: any
  gridSnappingLayoutConnectionBehavior: any
  gridSnappingResizeBehavior: any
  gridSnappingSpaceToolBehavior: any
  groupBehavior: any
  handTool: any
  hoverFix: any
  importDockingFix: any
  interactionEvents: any
  isHorizontalFix: any
  keyboard: any
  keyboardBindings: any
  keyboardMove: any
  keyboardMoveSelection: any
  labelBehavior: any
  labelEditingPreview: any
  labelEditingProvider: any
  labelSupport: any
  lassoTool: any
  layouter: any
  messageFlowBehavior: any
  minimap: any
  moddle: BpmnModdle
  moddleCopy: any
  modeling: Modeling
  modelingFeedback: any
  mouse: any
  move: any
  moveCanvas: any
  movePreview: any
  outline: any
  overlays: any
  palette: any
  paletteProvider: any
  pathMap: any
  popupMenu: any
  previewSupport: any
  removeElementBehavior: any
  removeParticipantBehavior: any
  replace: any
  replaceConnectionBehavior: any
  replaceElementBehaviour: any
  replaceMenuProvider: any
  resize: any
  resizeBehavior: any
  resizeHandles: any
  resizeLaneBehavior: any
  resizePreview: any
  resizeSnapping: any
  rootElementReferenceBehavior: any
  rules: any
  searchPad: any
  selection: Selection
  selectionBehavior: any
  selectionVisuals: any
  snapping: any
  spaceTool: any
  spaceToolBehavior: any
  spaceToolPreview: any
  styles: any
  subProcessStartEventBehavior: any
  textRenderer: any
  toggleElementCollapseBehaviour: any
  toolManager: any
  tooltips: any
  touchFix: any
  touchInteractionEvents: any
  translate: any
  unclaimIdBehavior: any
  unsetDefaultFlowBehavior: any
  updateFlowNodeRefsBehavior: any
  zoomScroll: any
}
