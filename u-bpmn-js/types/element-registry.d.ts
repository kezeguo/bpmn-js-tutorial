import { Shape, Root, Connection } from "./draw"

export interface _Element {
  /** 图形实例 */
  element: Shape | Root | Connection
  /** 元素实例 */
  gfx: SVGElement
  secondaryGfx: SVGElement
}

export interface ElementRegistry {
  _elements: { [id: string]: _Element }
  _eventBus: EventBus
  /** 添加一个元素 */
  add(element: Shape, gfx, secondaryGfx): void
  /** 根据函数条件过滤出指定的图形集合 */
  filter(fn: (elemet: Shape, gfx: SVGElement) => void): Array<Shape>
  /** 遍历所有图形元素 */
  forEach(fn: (elemet: Shape, gfx: SVGElement) => void): void
  /**
   * 获取一个图形元素
   * @param filter 元素id或者指定元素
   */
  get(filter: SVGElement | string): Shape
  /** 返回满足要求的第一个元素 */
  find(fn: (elemet: Shape, gfx: SVGElement) => void): Shape
  /** 获取所有图形元素 */
  getAll(): Shape[]
  /** 获取图形 */
  getGraphics(filter: Base, secondary: boolean): SVGElement
  /** 移除一个图形元素 */
  remove(element: Base): void
  /** 更新图形元素的ID */
  updateId(element: Base, newId: string): void
  /** 验证图形元素的ID */
  _validateId(id: string): void
}
