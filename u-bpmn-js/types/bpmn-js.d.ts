import { EventCallBack, EventName } from "./event"
import { Moddle, BpmnModdle } from 'bpmn-moddle'
import { Canvas } from './draw'

interface ImportXMLResult {
  warnings: Array<string>
}

interface ImportXMLError extends ImportXMLResult {

}

interface ImportDefinitionsResult extends ImportXMLResult {

}

interface ImportDefinitionsError extends ImportXMLResult {
}

interface OpenResult {

}
interface OpenError {

}

interface SaveXMLOptions {

}

interface BPMNOptions {

}

export interface Diagram {
  /**
   * @param {Object} options
   * @param {Array<didi.Module>} [options.modules] external modules to instantiate with the diagram
   * @param {didi.Injector} [injector] an (optional) injector to bootstrap the diagram with
   */
  constructor(options, injector)
}

export interface BaseViewer extends Diagram {
  _definitions: []
  _modules: []
  /**
   * BPMN 2.0图的基本查看器。
   *
   * Have a look at {@link Viewer}, {@link NavigatedViewer} or {@link Modeler} for
   * bundles that include actual features.
   *
   * @param {Object} [options] configuration options to pass to the viewer
   * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
   * @param {string|number} [options.width] the width of the viewer
   * @param {string|number} [options.height] the height of the viewer
   * @param {Object} [options.moddleExtensions] extension packages to provide
   * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
   * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
   */
  (options): BaseViewer
  /** 内部初始化使用 */
  _init(container, moddle, options): void
  /** 触发一个事件 */
  _emit(type: string, event: object): void
  /** 创建一个容器 */
  _createContainer(): void
  /** 创建一个Moddle */
  _createModdle(): void
  /**
   * Parse and render a BPMN 2.0 diagram.
   *
   * Once finished the viewer reports back the result to the
   * provided callback function with (err, warnings).
   *
   * ## Life-Cycle Events
   *
   * During import the viewer will fire life-cycle events:
   *
   *   * import.parse.start (about to read model from xml)
   *   * import.parse.complete (model read; may have worked or not)
   *   * import.render.start (graphical import start)
   *   * import.render.complete (graphical import finished)
   *   * import.done (everything done)
   *
   * You can use these events to hook into the life-cycle.
   *
   * @param xml the BPMN 2.0 xml
   * @param bpmnDiagram BPMN diagram or id of diagram to render (if not provided, the first one will be rendered)
   *
   */
  importXML(xml: string, bpmnDiagram?: ModdleElement | string): Promise<ImportXMLResult>
  /**
   * Import parsed definitions and render a BPMN 2.0 diagram.
   *
   * Once finished the viewer reports back the result to the
   * provided callback function with (err, warnings).
   *
   * ## Life-Cycle Events
   *
   * During import the viewer will fire life-cycle events:
   *
   *   * import.render.start (graphical import start)
   *   * import.render.complete (graphical import finished)
   *
   * You can use these events to hook into the life-cycle.
   *
   * @param definitions parsed BPMN 2.0 definitions
   * @param [bpmnDiagram] BPMN diagram or id of diagram to render (if not provided, the first one will be rendered)
   */
  importDefinitions(
    definitions: ModdleElement<Definitions>,
    bpmnDiagram?: ModdleElement<BPMNDiagram> | string
  ): Promise<ImportDefinitionsResult>
  /**
   * 打开以前导入的XML图。
   *
   * 完成后，查看器将结果报告给所提供的回调函数，并附带(err，警告)。
   *
   * ## 生命周期事件
   *
   * 在切换期间，viewer将触发生命周期事件:
   *
   *   * import.render.start (图形导入开始)
   *   * import.render.complete (图形导入完成)
   *
   * 可以使用这些事件与生命周期挂钩。
   *
   * @param [bpmnDiagramOrId] Id或要打开的图
   */
  open(bpmnDiagramOrId: string | ModdleElement<BPMNDiagram>): Promise<OpenResult, OpenError>
  /**
   * 将当前显示的BPMN 2.0关系图导出为BPMN 2.0 XML文档。
   *
   * ## 生命周期事件
   *
   * 在XML保存期间，viewer将触发生命周期事件:
   *
   *   * saveXML.start (在序列化之前)
   *   * saveXML.serialized (在xml生成)
   *   * saveXML.done (一切都完成)
   *
   * 可以使用这些事件与生命周期挂钩。
   *
   * @param {object} [options] export options
   * @param {boolean} [options.format=false] 是否输出经过格式化的xml
   * @param {boolean} [options.preamble=true] 是否输出xml声明
   */
  saveXML(options: { format: boolean, preamble: boolean }): Promise<{ xml: string }, Error>
  /**
   * 将当前显示的BPMN 2.0图导出为SVG图像。
   *
   * ## 生命周期事件
   *
   * 在SVG保存期间，viewer将触发生命周期事件:
   *
   *   * saveSVG.start (在序列化之前)
   *   * saveSVG.done (一切都完成)
   *
   * 可以使用这些事件与生命周期挂钩。
   */
  saveSVG(options): Promise<{ svg: string }, Error>
  getModules(): []
  /** 从查看器中删除所有绘制的元素。调用此方法后，查看器仍可重用以打开另一个图表。 */
  clear(): void
  /** 销毁查看器实例，并从文档树中删除它的所有剩余部分。 */
  destroy(): void
  /** 注册一个事件监听器 */
  on(event: EventName | EventName[], priority: number | EventCallBack, callback: EventCallBack, target: object): void
  /** 取消注册事件监听器 */
  off(event, callback): void
  /** 添加到指定dom节点中 */
  attachTo(parentNode: string | Node): void
  /** 从当前父节点中移除 */
  detach(): void
  _setDefinitions(definitions): void
  getDefinitions(): []
}

export interface BPMNViewer extends BaseViewer {
  /**
   * A viewer for BPMN 2.0 diagrams.
   *
   * Have a look at {@link NavigatedViewer} or {@link Modeler} for bundles that include
   * additional features.
   *
   *
   * ## Extending the Viewer
   *
   * In order to extend the viewer pass extension modules to bootstrap via the
   * `additionalModules` option. An extension module is an object that exposes
   * named services.
   *
   * The following example depicts the integration of a simple
   * logging component that integrates with interaction events:
   *
   *
   * ```javascript
   *
   * // logging component
   * function InteractionLogger(eventBus) {
   *   eventBus.on('element.hover', function(event) {
   *     console.log()
   *   })
   * }
   *
   * InteractionLogger.$inject = [ 'eventBus' ]; // minification save
   *
   * // extension module
   * var extensionModule = {
   *   __init__: [ 'interactionLogger' ],
   *   interactionLogger: [ 'type', InteractionLogger ]
   * };
   *
   * // extend the viewer
   * var bpmnViewer = new Viewer({ additionalModules: [ extensionModule ] });
   * bpmnViewer.importXML(...);
   * ```
   *
   * @param {Object} [options] configuration options to pass to the viewer
   * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
   * @param {string|number} [options.width] the width of the viewer
   * @param {string|number} [options.height] the height of the viewer
   * @param {Object} [options.moddleExtensions] extension packages to provide
   * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
   * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
   */
  (options): BPMNViewer
  _moddleExtensions: object
  _modules: []
}

export interface BaseModeler extends BaseViewer {
  /**
   * A base modeler for BPMN 2.0 diagrams.
   *
   * Have a look at {@link Modeler} for a bundle that includes actual features.
   *
   * @param {Object} [options] configuration options to pass to the viewer
   * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
   * @param {string|number} [options.width] the width of the viewer
   * @param {string|number} [options.height] the height of the viewer
   * @param {Object} [options.moddleExtensions] extension packages to provide
   * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
   * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
   */
  (options): BaseModeler
  /**
   * Create a moddle instance, attaching ids to it.
   *
   * @param {Object} options
   */
  _createModdle()
}
export interface BPMNModeler extends BaseModeler {
  (parameters): BaseModeler
}


// class UBpmnModeler extends Modeler {}
// class UBpmnViewer extends Viewer {}
// #endregion
