import { ModdleElement } from "."


interface ElementType {
  root
  label
  shape
  connection
}

/**
 * 工厂类
 */
export interface Factory {
  model: BpmnModdle
  properties: Properties
  createType(descriptor): void
}

export interface BpmnFactory {
  (moddle): BpmnFactory
  _model: BpmnModdle
  create(type, attrs)
  createDiBounds(bounds)
  createDiEdge(semantic, waypoints, attrs)
  createDiLabel()
  createDiPlane(semantic)
  createDiShape(semantic, bounds, attrs)
  createDiWaypoint(point)
  createDiWaypoints(waypoints)
  /** 给元素分配一个id */
  _ensureId(element: ModdleElement)
  /** 检测元素是否需要id */
  _needsId(element: ModdleElement)
}

/**
 * 元素工厂类
 */
export interface ElementFactory {
  _bpmnFactory: BpmnFactory
  _moddle: BpmnModdle
  _uid: Number
  /** 翻译函数 */
  _translate(template, replacements): string

  baseCreate(type, attrs): void
  create(elementType, attrs): void
  createBpmnElement(elementType, attrs): void
  /** 创建参与者 */
  createParticipantShape(attrs: boolean | Object): void
  _getDefaultSize(semantic): void

  /** 使用给定的类型和许多预先设置的属性创建一个模型元素。 */
  create(type: keyof ElementType | string, attrs: Object): Base
  createConnection(attrs: Object): Connection
  createLabel(attrs: Object): void
  createRoot(attrs: Object): Root
  createShape(attrs: Object): Shape
}