import { ElementTypes } from 'bpmn-moddle'
import { ElementRegistry } from "./element-registry"
import { BpmnModdle } from "./moddle"


export interface BmpnElementTypes extends ElementTypes {
  /** 标签 */
  label
}


interface Dimensions {
  width: number
  height: number
}
interface Point {
  x: number
  y: number
}
interface Bounds extends Dimensions, Point {
  scale: number
  inner: Dimensions & Point
  outer: Bounds
}

export interface Canvas {
  _container: HTMLElement
  _elementRegistry: ElementRegistry
  _eventBus: EventBus
  _graphicsFactory: object
  _layers: {}
  _svg: SVGSVGElement
  _viewboxChanged(): void
  _viewport: SVGGElement
  /**
   * The main drawing canvas.
   *
   * @class
   * @constructor
   *
   * @emits Canvas#canvas.init
   *
   * @param {Object} config
   * @param {EventBus} eventBus
   * @param {GraphicsFactory} graphicsFactory
   * @param {ElementRegistry} elementRegistry
   */
  (config, eventBus, graphicsFactory, elementRegistry): Canvas
  $inject: []
  _init(config): void
  _destroy(emit): void
  _clear(): void
  /**
   * 创建一个给定的层并返回它。
   *
   * @param {string} name
   * @param {number} [index=0]
   *
   * @return {Object} 层描述符与 { index, group: SVGGroup }
   */
  _createLayer(): Object
  _updateMarker(element, marker, add): void
  _ensureValid(type, element): void
  _setParent(element, parent, parentIndex): void
  /**
   * 向画布添加元素。
   *
   * 这将在元素和显式指定的父元素或隐式根元素之间连接parent <->子元素关系。
   *
   * 在添加过程中，它会发出事件
   *
   *  * <{type}.add> (element, parent)
   *  * <{type}.added> (element, gfx)
   *
   * 扩展可以钩入这些事件来执行它们的魔法。
   *
   * @param {string} type
   * @param {Object|djs.model.Base} element
   * @param {Object|djs.model.Base} [parent]
   * @param {number} [parentIndex]
   *
   * @return {Object|djs.model.Base} the added element
   */
  _addElement(type, element, parent, parentIndex): BaseShape
  _removeElement(element, type): BaseShape
  /**
   * viewbox变化时将执行新的函数。
   *
   * @param {Function} changeFn
   */
  _changeViewbox(changeFn): void
  _viewboxChanged(): void
  _fitViewport(center): void
  _setZoom(scale, center): void
  /**  触发一个事件，以便其他模块可以对画布的调整做出反应 */
  resized(): void
  /** 返回画布的大小 */
  getSize(): Dimensions
  /**
   * 返回给定元素的绝对边界框
   *
   * 绝对边界框可以用于在调用者(浏览器)坐标系统中显示叠加，而不是用于放大/缩小画布坐标。
   *
   */
  getAbsoluteBBox(element: SVGElement): Bounds
  /**
   * 获取或设置画布的视图框，即当前显示的区域。
   *
   * getter可能会返回一个缓存的viewbox(如果它正在改变)。为了强制重新计算，传递' false '作为第一个参数。
   * @example
   *
   * canvas.viewbox({ x: 100, y: 100, width: 500, height: 500 })
   *
   * // 设置图表的可见区域为(100|100)->(600|100)，并根据图表的宽度缩放它
   *
   * var viewbox = canvas.viewbox(); // 传递' false '强制重新计算框。
   *
   * console.log(viewbox);
   * // {
   * //   inner: Dimensions,
   * //   outer: Dimensions,
   * //   scale,
   * //   x, y,
   * //   width, height
   * // }
   *
   * // 如果当前图表被缩放和滚动，也可以通过此方法将其重置为默认缩放:
   *
   * var zoomedAndScrolledViewbox = canvas.viewbox();
   *
   * canvas.viewbox({
   *   x: 0,
   *   y: 0,
   *   width: zoomedAndScrolledViewbox.outer.width,
   *   height: zoomedAndScrolledViewbox.outer.height
   * });
   */
  viewbox(box: Bounds): Bounds
  viewbox(): Bounds
  /**
   * 获取或设置画布的滚动。
   */
  scroll(delta: { dx: number; dy: number }): void
  /**
   * 获取或设置画布的当前缩放，可以选择缩放到指定位置。
   *
   * getter可能会返回一个缓存的缩放级别。使用' false '作为第一个参数调用它来强制重新计算当前级别。
   *
   * @param [newScale] 新的缩放级别，可以是一个数字，即0.9，或“fit-viewport”来调整大小以适应当前的视口
   
   */
  zoom(newScale: 'fit-viewport' | 'fit-content' | number, center: 'auto' | Point): number
  /**
   * 向画布添加形状
   */
  addShape(shape: object | Shape, parent: BaseShape, parentIndex: number): Shape
  /**
   * 从画布上删除一个形状
   * @param shape 需要移除的形状或形状id
   */
  removeShape(shape: string | Shape): Shape
  /**
   * 向画布添加一根连线
   */
  addConnection(connection: object | Connection, parent: BaseShape, parentIndex: number): Connection
  /**
   * 从画布中删除连接
   * @param -[connection] connection or connection id to be removed
   */
  removeConnection(connection: string | Connection): Connection
  /**
   * 向元素添加标记(基本上是一个css类)。
   *
   * 触发element.marker.update事件，使得将扩展集成到标记生命周期也成为可能。
   *
   * @example
   * canvas.addMarker('foo', 'some-marker');
   *
   * var fooGfx = canvas.getGraphics('foo');
   *
   * fooGfx; // <g class="... some-marker"> ... </g>
   */
  addMarker(element: string | BaseShape, marker: string): void
  /**
   * 从元素中移除标记。
   *
   * 触发element.marker.update事件，使得将扩展集成到标记生命周期也成为可能。
   *
   * @param  {string|djs.model.Base} element
   * @param  {string} marker
   */
  removeMarker(element: string | BaseShape, marker: string): void
  /**
   * 检查元素上是否存在标记。
   */
  hasMarker(element: string | BaseShape, marker: string): boolean
  /**
   * 切换元素上的标记。
   *
   * 触发element.marker.update事件，使得将扩展集成到标记生命周期也成为可能。
   */
  toggleMarker(element: string | BaseShape, marker: string): void
  getRootElement(): HTMLElement
  /**
   * 将给定元素设置为画布的新根元素，并返回新的根元素。
   *
   * @param [override] 是否覆盖当前根元素(如果有的话)
   */
  setRootElement(element: Root, override: boolean): Root
  /**
   * 返回绘制所有元素的默认图层。
   */
  getDefaultLayer(): SVGElement
  /**
   * 返回用于在其上绘制元素或注释的层。
   *
   * 通过此方法检索的不存在的层将被创建。在创建过程中，可选索引可用于创建在现有层之上或之下的层。具有特定索引的层总是创建在具有相同索引的所有现有层之上。
   */
  getLayer(name: string, index: number): SVGElement
  /**
   * 返回包含绘图画布的html元素。
   */
  getContainer(): HTMLElement
  /**
   * 返回位于某个图表元素下的图形对象
   *
   * @param [secondary=false] 是否返回二次连接的元素
   */
  getGraphics(element: string | BaseShape, secondary: boolean): SVGElement
}
/**
 * 集合类
 */
export interface Collection extends Array {
  /**
   * 一个简单的标记，将该元素标识为refs集合
   */
  __refs_collection: boolean
  add(element: object, idx: number): void
  /**
   * 是否包含这个元素
   */
  contains(element: object): boolean
  remove(element: object): object
}
/**
 * 孩子集合类
 */
export interface Children extends Collection {
  [n: number]: Connection | Shape | Label
}

/**
 * 连接器集合类
 */
export interface Attachers extends Collection {
  [n: number]: Connection
}

export interface BaseModdleElement {
  /**
   * 调用 this.$model.properties.set
   */
  set(name, value): void
  /**
   * 调用 this.$model.properties.get
   */
  get(name): void
}

/**
 * 不同类型的元素其ModdleElement结构也不一致
 */
export interface ModdleElement extends BaseModdleElement {
  /**
   * 元素ID
   */
  id: string
  /**
   * 节点
   * @see http://cn.voidcc.com/question/p-fhfpvvaa-bay.html
   */
  di: ModdleElement
  /** 元素类型 */
  $type: keyof BmpnElementTypes
  /**
   * 属性
   */
  $attrs: object
  /**
   * 父元素
   */
  $parent: ModdleElement
  $instanceOf(element, type): boolean

  $model: BpmnModdle
  /** 元素的定义描述数据，见Factory.prototype.createType方法 */
  $descriptor: object

  // #region bpmn:Definitions特有属性
  /**
   *
   */
  diagrams: ModdleElement[]
  /**
   *
   */
  rootElements: ModdleElement[]
  /**
   *
   */
  targetNamespace: string
  // #endregion

  // #region 特有属性
  // #endregion


  // #region bpmn:Process特有属性
  /**
   * 流元素
   */
  flowElements: ModdleElement[]
  /**
   *
   */
  isExecutable: boolean
  // #endregion

  // #region bpmndi:BPMNPlane特有属性
  /**
   * 平面元素
   */
  planeElement: ModdleElement[]
  // #endregion
}

/**
 * 基本图形类
 */
export interface BaseShape {
  /**
   * 业务对象
   */
  businessObject: ModdleElement
  /**
   * 单标签支持，将映射到多标签数组
   */
  label
}
/**
 * 形状类
 */
export interface Shape extends BaseShape {
  /**
   * 连接器
   */
  attachers: Attachers
  /**
   * 主键
   */
  id: string
  /**
   * 宽度
   */
  width: number
  /**
   * 高度
   */
  height: number
  /**
   * 是否隐藏
   */
  hidden: boolean
  x: number
  y: number
  /**
   * bpmn元素类型
   */
  type: keyof BmpnElementTypes
  /**
   * 子元素
   */
  children: Children
  /**
   * 是否已合并
   */
  collapsed: boolean
  isFrame: boolean
  host
  label: Label
  labels: Label[]
  /**
   * 传入的流
   */
  incoming: Attachers
  /**
   * 传出的流
   */
  outgoing: Attachers
  /**
   * 父节点
   */
  parent: Root | Shape
}

/**
 * 标签文本类
 */
export interface Label extends Shape {
  labelTarget: Shape | Connection | BaseShape
}

/**
 * 连接线类
 */
export interface Connection extends Shape {
  /**
   * 源节点
   */
  source: Shape
  /**
   * 目标节点
   */
  target: Shape | Label
}

export interface Root extends Shape { }