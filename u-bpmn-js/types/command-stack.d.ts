
export interface Injector {
  (modules, parent): void
  createChild(modules, forceNewInstances)
  get(name, strict)
  instantiate(Type)
  invoke(func, context, locals)
  _instances: Object
  _providers: Object
}
export type Stack = {
  command: string
  context: object
  id: number
}
/**
 * 命令堆栈类，用于管理撤销（Ctrl+Z）、撤销上一次操作（Ctrl+Y）
 *
 * 一种提供不可执行和可执行命令的服务。
 */
export interface CommandStack {
  (eventBus: EventBus, injector: Injector): CommandStack
  _currentExecution: {
    actions: Array
    atomic: boolean
    dirty: Array
  }
  _eventBus: EventBus
  _handlerMap: object
  _injector: Injector
  _stack: Stack[]
  /**
   * 当前堆栈ID
   */
  _stackIdx: number
  _uid: number

  /** 判断指定命令是否可以执行 */
  canExecute(command: string, context: object): boolean
  /** 是否可以重做 */
  canRedo(): boolean
  /** 是否可以撤销 */
  canUndo(): boolean
  /**
   * 清除命令栈，清除所有撤消/重做的历史记录
   * @param emit 是否触发事件
   */
  clear(emit: boolean): void
  /** 执行一个命令 */
  execute(command: string, context: object): void
  /** 重做最后一个命令 */
  redo(): void
  /** 注册一个命令和处理函数 */
  register(command: string, handler: CommandHandler): void
  /** 向已有命令注册一个处理函数 */
  registerHandler(command: string, handlerCls): void
  /** 撤消最后一个命令(s) */
  undo(): void

  // 以下内部函数
  _atomicDo(fn): void
  _createId(): void
  _executedAction(action, redo): void
  _fire(command, qualifier, event): void
  _getHandler(command): void
  _getRedoAction(): void
  _getUndoAction(): void
  _internalExecute(action, redo): void
  _internalUndo(action): void
  _markDirty(elements): void
  _popAction(): void
  _pushAction(action): void
  _revertedAction(action): void
  _setHandler(command, handler: CommandHandler): void
}
