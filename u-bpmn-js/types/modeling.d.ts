import { CommandStack } from "./command-stack"
import { Shape } from "./draw"
import { EventBus } from "./event"
import { ElementFactory } from "./factory"

export type AlignmentType = 'top' | 'right' | 'bottom' | 'left' | 'middle' | 'center'

/** 可用于将插件插入命令执行以进行扩展和/或验证的实用程序。 */
export interface CommandInterceptor {
  (eventBus: EventBus): CommandInterceptor
  _eventBus: EventBus
  canExecute(events, priority, handlerFn, unwrap, that)
  execute(events, priority, handlerFn, unwrap, that)
  executed(events, priority, handlerFn, unwrap, that)
  on(events, hook, priority, handlerFn, unwrap, that)
  postExecute(events, priority, handlerFn, unwrap, that)
  postExecuted(events, priority, handlerFn, unwrap, that)
  preExecute(events, priority, handlerFn, unwrap, that)
  preExecuted(events, priority, handlerFn, unwrap, that)
  revert(events, priority, handlerFn, unwrap, that)
  reverted(events, priority, handlerFn, unwrap, that)
}
/**
 * 可扩展以实现建模规则的基本提供程序。
 *
 * 扩展应该实现init方法来实际添加自定义建模检查。可以通过#addRule(action, fn)方法添加检查。
 */
export interface RuleProvider extends CommandInterceptor {
  (eventBus: EventBus): RuleProvider
  init(): void
  /**
   * 为给定操作添加建模规则，通过回调函数实现
   *
   * 该函数将接收特定于建模的操作上下文来执行检查。它必须返回' false '以禁止操作发生，或' true '以允许操作发生。
   *
   * 规则提供程序可以通过不返回任何结果(或undefined)的方式将计算传递给较低优先级的规则。
   * @example
   *
   * ResizableRules.prototype.init = function() {
   *
   *   // Return `true`, `false` or nothing to denote _allowed_, _not allowed_ and _continue evaluating_.
   *   this.addRule('shape.resize', function(context) {
   *
   *     var shape = context.shape;
   *
   *     if (!context.newBounds) {
   *       // check general resizability
   *       if (!shape.resizable) {
   *         return false;
   *       }
   *
   *       // not returning anything (read: undefined)
   *       // will continue the evaluation of other rules
   *       // (with lower priority)
   *       return;
   *     } else {
   *       // element must have minimum size of 10*10 points
   *       return context.newBounds.width > 10 && context.newBounds.height > 10;
   *     }
   *   });
   */
  addRule(actions, priority, fn): void
}
/** BPMN特定的建模规则 */
export interface BpmnRules {
  (eventBus: EventBus): BpmnRules
  canAttach(elements, target, source, position): boolean
  canConnect(source, target, connection): boolean
  canConnectAssociation(source, target): boolean
  canConnectDataAssociation(source, target): boolean
  canConnectMessageFlow(source, target): boolean
  canConnectSequenceFlow(source, target): boolean
  canCopy(elements, element): boolean
  canCreate(shape, target, source, position): boolean
  canDrop(element, target, position): boolean
  canInsert(shape, flow, position): boolean
  canMove(elements, target): boolean
  canReplace(elements, target, position): boolean
  canResize(shape, newBounds): boolean
}

/** 建模基础类 */
export interface BaseModeling {
  (eventBus: EventBus, elementFactory: ElementFactory, commandStack: CommandStack): BaseModeling

  alignElements(elements, alignment): void
  appendShape(source, shape, position, target, hints): Shape
  connect(source, target, attrs, hints): void
  createConnection(source, target, parentIndex, connection, parent, hints): void
  createElements(elements, position, parent, parentIndex, hints): void
  createLabel(labelTarget, position, label, parent): Shape
  createShape(shape, position, target, parentIndex, hints): void
  createSpace(movingShapes, resizingShapes, delta, direction, start): void
  distributeElements(groups, axis, dimension): void
  /**
   * 获取所有事件处理函数
   */
  getHandlers(): void
  layoutConnection(connection, hints): void
  moveConnection(connection, delta, newParent, newParentIndex, hints)
  /**
   * 将多个形状移动到新目标，可以将其设置为新的父对象，也可以将其附加。
   * @param shapes
   * @param delta
   * @param target
   * @param hints
   */
  moveElements(shapes: Shape[], delta: { x: number; y: number }, target, hints): void
  moveShape(shape, delta, newParent, newParentIndex, hints): void
  reconnect(connection, source, target, dockingOrPoints, hints): void
  reconnectEnd(connection, newTarget, dockingOrPoints, hints): void
  reconnectStart(connection, newSource, dockingOrPoints, hints): void
  /**
   * 在命令栈中注册处理程序
   * @param commandStack
   */
  registerHandlers(commandStack: CommandStack): void
  removeConnection(connection, hints): void
  removeElements(elements): void
  removeShape(shape, hints): void
  replaceShape(oldShape, newShape, hints): void
  resizeShape(shape, newBounds, minBounds, hints): void
  toggleCollapse(shape, hints)
  /**
   * 更新给定形状的附件
   */
  updateAttachment(shape: Shape, newHost)
  updateWaypoints(connection, newWaypoints, hints)
  _create(type, attrs)
}

/** BPMN 2.0建模特性activator */
export interface Modeling extends BaseModeling {
  (eventBus: EventBus, elementFactory: ElementFactory, commandStack: CommandStack, bpmnRules: BpmnRules): BaseModeling
  _bpmnRules
  _commandStack: CommandStack
  _elementFactory: ElementFactory
  _eventBus: EventBus
  addLane(targetLaneShape, location): void
  claimId(id, moddleElement): void
  connect(source, target, attrs, hints): void
  /** 获取所有事件处理函数 */
  getHandlers(): void
  /** 将当前图转换为集合组 */
  makeCollaboration(): Root
  /** 将当前图转换为流程 */
  makeProcess(): Root
  resizeLane(laneShape, newBounds, balanced): void
  setColor(elements, colors): void
  splitLane(targetLane, count): void
  unclaimId(id, moddleElement): void
  updateLabel(element, newLabel, newBounds, hints): void
  updateLaneRefs(flowNodeShapes, laneShapes): void
  updateProperties(element, properties): void

  alignElements(elements: Shape[], alignment: AlignmentType)
  appendShape(source, shape, position, target, hints)
  connect(source, target, attrs, hints)
  createConnection(source, target, parentIndex, connection, parent, hints)
  createElements(elements, position, parent, parentIndex, hints)
  createLabel(labelTarget, position, label, parent)
  createShape(shape, position, target, parentIndex, hints)
  createSpace(movingShapes, resizingShapes, delta, direction, start)
  distributeElements(groups, axis, dimension)
  getHandlers()
  layoutConnection(connection, hints)
  moveConnection(connection, delta, newParent, newParentIndex, hints)
  moveElements(shapes, delta, target, hints)
  moveShape(shape, delta, newParent, newParentIndex, hints)
  reconnect(connection, source, target, dockingOrPoints, hints)
  reconnectEnd(connection, newTarget, dockingOrPoints, hints)
  reconnectStart(connection, newSource, dockingOrPoints, hints)
  registerHandlers(commandStack)
  removeConnection(connection, hints)
  removeElements(elements)
  removeShape(shape, hints)
  replaceShape(oldShape, newShape, hints)
  resizeShape(shape, newBounds, minBounds, hints)
  toggleCollapse(shape, hints)
  updateAttachment(shape, newHost)
  updateWaypoints(connection, newWaypoints, hints)
  _create(type, attrs)
}