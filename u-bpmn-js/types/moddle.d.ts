
export interface Seed extends Function {
  (data): string
  bits: number
  base: number
  hats: { [id: string]: ModdleElement }
  get(id): ModdleElement
  set(id, value: ModdleElement): this
}
/**
 * id生成器/缓存类
 */
export interface Ids {
  /** 创建一个新的id生成器/缓存实例, 您可以选择提供一个内部使用的种子。 */
  (seed: Seed): void
  _seed: Seed
  /** 检查id是否已被分配 */
  assigned(id: string): boolean
  /** 手动申请一个现有id  */
  claim(id: string, element: ModdleElement): void
  /** 释放一个id */
  unclaim(id: string): void
  /** 清空所有已声明的id */
  clear(): void
  /** 生成下一个id */
  next(element: Object): string
  /** 生成一个具有指定前缀的id */
  nextPrefixed(prefix: string, element: object): string
}

/** 获取和设置模型元素属性的工具类 */
export interface Properties {
  (model: Model): void
  model: BpmnModdle
  /**
   * 在目标元素上定义属性
   */
  define(target: object, name: string, options: object): void
  /**
   * 定义元素的描述符
   */
  defineDescriptor(target: object, descriptor): void
  /**
   * 定义元素的模型
   */
  defineModel(target, model): void
  /**
   * 返回给定元素的命名属性
   */
  get(target: object, name: string): object
  /**
   * 设置目标元素上的命名属性, 如果该值未定义，则删除该属性
   */
  set(target: object, name: string, value: object): void
}

type package = {
  name: string
  prefix: string
  types: Array<{ name: string }>
}

export interface Registry {
  /**
   * @typedef packageItem
   * @property {Array} associations
   * @property {Array} enumerations
   * @property {String} name
   * @property {String} prefix
   * @property {Array} types
   * @property {String} uri
   * @property {{tagAlias:String,typePrefix:String}} xml
   */
  /**
   * @type {Object}
   */
  packageMap
  /**
   * @type {Array<packageItem>}
   */
  packages
  /**
   * @type {Properties}
   */
  properties

  /**
   * @type {Object}
   */
  typeMap
  definePackage(target, pkg): void
  getEffectiveDescriptor(name): void
  getPackage(uriOrPrefix): void
  getPackages(): void
  mapTypes(nsName, iterator, trait): void
  registerPackage(pkg): void
  registerType(type, pkg): void
}

type SerializationResult = {
  xml: string
}
type ParseResult = {
  rootElement: ModdleElement
  references: []
  warnings: Error[]
  /**
   * 一个包含每个ID -> ModdleElement的映射
   */
  elementsById: object
}

/** 可以用来创建特定类型元素的模型。 */
export interface Moddle {
  (packages: package[]): void
  /**
   * 创建指定类型的实例  `var foo = moddle.create('my:Foo');var bar = moddle.create('my:Bar', { id: 'BAR_1' });`
   */
  create(descriptor: string | object, attrs: object): object
  /** 创建在模型实例中使用的任意元素类型
   *
   * var foo = moddle.createAny('vendor:Foo', 'http://vendor', {
   *   value: 'bar'
   * });
   *
   * var container = moddle.create('my:Container', 'http://my', {
   *   any: [ foo ]
   * }); */
  createAny(name: string, nsUri: string, properties: object): object
  /** 返回元素的描述符 */
  getElementDescriptor(element): void
  /**
   * 按uri或前缀返回已注册的包
   * @param uriOrPrefix
   */
  getPackage(uriOrPrefix: string): object
  /**
   * 返回所有已知包的快照
   */
  getPackages(): object
  /**
   * 返回名为属性的元素的描述符
   * @param element
   * @param property
   */
  getPropertyDescriptor(element, property): void
  /**
   * 返回表示给定描述符的类型
   */
  getType(descriptor: string | object): object
  /**
   * 返回映射类型的描述符
   * @param type
   */
  getTypeDescriptor(type): void
  /**
   * 如果给定描述符或实例表示给定类型，则返回true。
   *
   * 如果'element'被省略, 会使用'this'。
   * @param element
   * @param type
   */
  hasType(element, type): void
}

/** Moddle的子类, 支持bpmn2.0 xml文件的导入和导出。 */
export interface BpmnModdle extends Moddle {
  (packages: object | [], options: object): Moddle
  /**
   * 从给定的xml字符串实例化BPMN模型树。
   * @param xmlStr xml字符串
   * @param typeName 根元素的名称
   * @param options 要传递给底层读取器的选项
   */
  fromXML(xmlStr: string, typeName, options): Promise<ParseResult, ParseError>
  /**
   * 将BPMN 2.0对象树序列化为XML。
   * @param element 根元素，通常是' bpmn:Definitions '的实例
   * @param options 要传递的选项
   */
  toXML(element, options): Promise<SerializationResult, Error>
  /**
   * 工厂
   */
  factory: Factory
  /**
   * id管理器
   */
  ids: Ids
  /**
   * 属性管理器
   */
  properties: Properties
  /**
   * 注册器
   */
  registry: Registry
  /**
   * 类型缓存
   */
  typeCache: { [id: string]: ModdleElement }
}