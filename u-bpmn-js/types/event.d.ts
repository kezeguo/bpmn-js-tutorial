
export type EventCallBack = (e: InternalEvent) => void
export type EventHandle = {
  callback: EventCallBack
  next: EventHandle
  /** 优先级 */
  priority: number
}


export type EventName = keyof (BPMNEventMap & BPMNCustomEventMap)

/**
 * 内部事件类
 */
export interface InternalEvent {
  /**
   * 事件类型
   */
  type: string
  init(data): void
  preventDefault(): void
  stopPropagation(): void
}

export interface EventBus {
  _listeners: {
    [eventName: EventName]: EventHandle
  }
  /** 创建一个事件 */
  createEvent(data): void
  /** 触发某个事件 */
  fire(type: string, data: any): void
  /** 停止监听某一事件或事件集合 */
  off(events: EventName | EventName[], callback: EventCallBack): void

  /** 监听某一事件或事件集合 */
  on(events: EventName | EventName[], callback: EventCallBack): void
  /** 监听某一事件或事件集合 */
  on(events: EventName | EventName[], callback: EventCallBack, that: object): void
  /** 监听某一事件一次 */
  on(event: EventName | EventName[], priority: number, callback: EventCallBack): void
  /** 监听某一事件或事件集合 */
  on(events: EventName | EventName[], priority: number, callback: EventCallBack, that: object): void

  /** 监听某一事件一次 */
  once(event: EventName | EventName[], callback: EventCallBack): void
  /** 监听某一事件一次 */
  once(event: EventName | EventName[], callback: EventCallBack, that: object): void
  /** 监听某一事件一次 */
  once(event: EventName | EventName[], priority: number, callback: EventCallBack): void
  /** 监听某一事件一次 */
  once(event: EventName | EventName[], priority: number, callback: EventCallBack, that: object): void

  /** 处理事件错误 */
  handleError(error: EventCallBack): void

  // 以下为内部调用方法
  _addListener(event: string, newListener: EventCallBack): void
  _destroy(): void
  _getListeners(name): void
  _invokeListener(event, args, listener): void
  _invokeListeners(event, args, listener): void
  _removeListener(event, callback): void
  _setListeners(name, listener): void
}

export interface BPMNCustomEventMap {
  'import.finshed'
}

/** 事件名称列表（大部分，不是所有的） */
export interface BPMNEventMap {
  attach
  autoPlace
  'autoPlace.end'
  'autoPlace.start'
  'bendpoint.move.cancel'
  'bendpoint.move.cleanup'
  'bendpoint.move.end'
  'bendpoint.move.hover'
  'bendpoint.move.move'
  'bendpoint.move.out'
  'bendpoint.move.start'
  'bpmnElement.added'
  // #region 命令堆栈相关
  'commandStack.canvas.updateRoot.executed'
  'commandStack.canvas.updateRoot.postExecute'
  'commandStack.canvas.updateRoot.preExecute'
  'commandStack.canvas.updateRoot.reverted'
  'commandStack.changed'
  'commandStack.connection.create.canExecute'
  'commandStack.connection.create.executed'
  'commandStack.connection.create.postExecute'
  'commandStack.connection.create.postExecuted'
  'commandStack.connection.create.preExecute'
  'commandStack.connection.create.preExecuted'
  'commandStack.connection.create.reverted'
  'commandStack.connection.delete.executed'
  'commandStack.connection.delete.preExecute'
  'commandStack.connection.delete.reverted'
  'commandStack.connection.layout.executed'
  'commandStack.connection.layout.postExecute'
  'commandStack.connection.layout.postExecuted'
  'commandStack.connection.layout.reverted'
  'commandStack.connection.move.executed'
  'commandStack.connection.move.preExecute'
  'commandStack.connection.move.reverted'
  'commandStack.connection.reconnect.canExecute'
  'commandStack.connection.reconnect.executed'
  'commandStack.connection.reconnect.postExecute'
  'commandStack.connection.reconnect.preExecute'
  'commandStack.connection.reconnect.reverted'
  'commandStack.connection.start.canExecute'
  'commandStack.connection.updateWaypoints.canExecute'
  'commandStack.connection.updateWaypoints.executed'
  'commandStack.connection.updateWaypoints.postExecute'
  'commandStack.connection.updateWaypoints.postExecuted'
  'commandStack.connection.updateWaypoints.reverted'
  'commandStack.element.autoResize.canExecute'
  'commandStack.element.copy.canExecute'
  'commandStack.element.updateAttachment.executed'
  'commandStack.element.updateAttachment.reverted'
  'commandStack.element.updateProperties.postExecute'
  'commandStack.element.updateProperties.postExecuted'
  'commandStack.elements.create.canExecute'
  'commandStack.elements.create.postExecute'
  'commandStack.elements.create.postExecuted'
  'commandStack.elements.create.preExecute'
  'commandStack.elements.create.revert'
  'commandStack.elements.delete.postExecuted'
  'commandStack.elements.delete.preExecute'
  'commandStack.elements.move.canExecute'
  'commandStack.elements.move.postExecuted'
  'commandStack.elements.move.preExecute'
  'commandStack.elements.move.preExecuted'
  'commandStack.label.create.postExecute'
  'commandStack.label.create.postExecuted'
  'commandStack.lane.add.postExecuted'
  'commandStack.lane.add.preExecute'
  'commandStack.lane.resize.postExecuted'
  'commandStack.lane.resize.preExecute'
  'commandStack.lane.split.postExecuted'
  'commandStack.lane.split.preExecute'
  'commandStack.shape.append.preExecute'
  'commandStack.shape.attach.canExecute'
  'commandStack.shape.create.canExecute'
  'commandStack.shape.create.execute'
  'commandStack.shape.create.executed'
  'commandStack.shape.create.postExecute'
  'commandStack.shape.create.postExecuted'
  'commandStack.shape.create.preExecute'
  'commandStack.shape.create.revert'
  'commandStack.shape.create.reverted'
  'commandStack.shape.delete.execute'
  'commandStack.shape.delete.executed'
  'commandStack.shape.delete.postExecute'
  'commandStack.shape.delete.postExecuted'
  'commandStack.shape.delete.preExecute'
  'commandStack.shape.delete.revert'
  'commandStack.shape.delete.reverted'
  'commandStack.shape.move.executed'
  'commandStack.shape.move.postExecute'
  'commandStack.shape.move.postExecuted'
  'commandStack.shape.move.preExecute'
  'commandStack.shape.move.reverted'
  'commandStack.shape.replace.postExecute'
  'commandStack.shape.replace.postExecuted'
  'commandStack.shape.replace.preExecuted'
  'commandStack.shape.resize.canExecute'
  'commandStack.shape.resize.executed'
  'commandStack.shape.resize.postExecute'
  'commandStack.shape.resize.postExecuted'
  'commandStack.shape.resize.preExecute'
  'commandStack.shape.resize.reverted'
  'commandStack.shape.toggleCollapse.executed'
  'commandStack.shape.toggleCollapse.postExecuted'
  'commandStack.shape.toggleCollapse.reverted'
  'commandStack.spaceTool.postExecuted'
  'commandStack.spaceTool.preExecute'
  // #endregion

  // #region 导入导出相关
  'import.parse.start'
  'import.parse.complete'
  'import.render.start'
  'import.render.complete'
  'import.done'
  'saveXML.start'
  'saveXML.serialized'
  'saveXML.done'
  'saveSVG.start'
  'saveSVG.done'
  // #endregion

  // #region 画布相关
  'canvas.destroy'
  'canvas.init'
  'canvas.resized'
  'canvas.viewbox.changed'
  'canvas.viewbox.changing'

  'resize.cleanup'
  'resize.end'
  'resize.move'
  'resize.start'
  // #endregion

  // #region 工具类
  'tool-manager.update'

  'hand.canceled'
  'hand.end'
  'hand.ended'
  'hand.init'
  'hand.move.canceled'
  'hand.move.end'
  'hand.move.ended'
  'hand.move.move'

  'lasso.canceled'
  'lasso.cleanup'
  'lasso.end'
  'lasso.ended'
  'lasso.move'
  'lasso.selection.canceled'
  'lasso.selection.end'
  'lasso.selection.ended'
  'lasso.selection.init'
  'lasso.start'

  'copyPaste.copyElement'
  'copyPaste.pasteElement'
  'copyPaste.pasteElements'

  'spaceTool.canceled'
  'spaceTool.cleanup'
  'spaceTool.end'
  'spaceTool.ended'
  'spaceTool.getMinDimensions'
  'spaceTool.move'
  'spaceTool.selection.canceled'
  'spaceTool.selection.cleanup'
  'spaceTool.selection.end'
  'spaceTool.selection.ended'
  'spaceTool.selection.init'
  'spaceTool.selection.move'
  'spaceTool.selection.start'
  // #endregion

  // #region 面板
  'contextPad.create'
  'contextPad.getProviders'

  'palette.create'
  'palette.getProviders'
  // #endregion

  // #region 连接器、连接线
  'global-connect.canceled'
  'global-connect.cleanup'
  'global-connect.drag.canceled'
  'global-connect.drag.ended'
  'global-connect.end'
  'global-connect.ended'
  'global-connect.hover'
  'global-connect.init'
  'global-connect.out'

  'connect.cleanup'
  'connect.end'
  'connect.hover'
  'connect.move'
  'connect.out'
  'connect.start'

  'connection.added'
  'connection.changed'
  'connection.remove'
  'connection.removed'

  'connectionSegment.move.cancel'
  'connectionSegment.move.cleanup'
  'connectionSegment.move.end'
  'connectionSegment.move.hover'
  'connectionSegment.move.move'
  'connectionSegment.move.out'
  'connectionSegment.move.start'
  // #endregion

  // #region 元素相关
  /** 元素被修改 */
  'element.changed'
  /** 元素被点击 */
  'element.click'
  /** 元素被双击 */
  'element.dblclick'
  /** 元素有焦点 */
  'element.hover'
  'element.marker.update'
  /** 元素鼠标按下 */
  'element.mousedown'
  /** 元素鼠标移动 */
  'element.mousemove'
  'element.out'
  'element.updateId'
  'elements.changed'
  'elements.delete'
  'elements.paste.rejected'
  // #endregion

  'create.cleanup'
  'create.end'
  'create.hover'
  'create.init'
  'create.move'
  'create.out'
  'create.rejected'
  'create.start'
  detach
  'diagram.clear'
  'diagram.destroy'
  'diagram.init'
  'directEditing.activate'
  'directEditing.cancel'
  'directEditing.complete'
  'directEditing.resize'
  'drag.cleanup'
  'drag.init'
  'drag.move'
  'drag.start'
  'editorActions.init'
  'i18n.changed'
  'interactionEvents.createHit'
  'interactionEvents.updateHit'
  'keyboard.keydown'
  'moddleCopy.canCopyProperties'
  'moddleCopy.canCopyProperty'
  'moddleCopy.canSetCopiedProperty'
  'popupMenu.getProviders.bpmn-replace'
  'popupMenu.open'
  'render.connection'
  'render.getConnectionPath'
  'render.getShapePath'
  'render.shape'
  /** 当前选集改变（实际上，每次鼠标点击都会触发） */
  'selection.changed'
  /** 图形被添加 */
  'shape.added'
  /** 图形被修改 */
  'shape.changed'
  'shape.move.cleanup'
  'shape.move.end'
  'shape.move.hover'
  'shape.move.init'
  'shape.move.move'
  'shape.move.out'
  'shape.move.rejected'
  'shape.move.start'
  'shape.remove'
  'shape.removed'
}