
/**
 * 
 * @param {*} val 
 * @param {Boolean} strict 是否严格判断
 */
export function isObject(val, strict = false) {
  return typeof val === 'object' && (strict ? val.constructor === Object : true)
}

export function isArray(val) {
  return typeof val === 'object' && val instanceof Array
}
export function isNull(val) {
  return typeof val == 'undefined'
}
