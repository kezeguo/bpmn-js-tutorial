import zhCn from './zh-CN'
// https://github.com/bpmn-io/bpmn-js-examples/tree/master/i18n/app/customTranslate
// https://github.com/bpmn-io/bpmn-js-i18n/blob/master/translations/zn.js (不全，很多没翻译过来)

const translations = {
  'zh-CN': zhCn
}

export default function bpmnTranslation(lang = 'zh-CN') {
  return {
    translate: [
      'value',
      function customTranslate(template, replacements) {
        replacements = replacements || {}

        // 找到目标语言对应的字符串
        template = translations[lang]?.[template] ?? template

        // 替换文本
        return template.replace(/{([^}]+)}/g, function (_, key) {
          return replacements[key] || '{' + key + '}'
        })
      }
    ]
  }
}
